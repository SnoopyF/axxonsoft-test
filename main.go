package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

type AxonSoftTestApp struct {
	storage Storage
	client  *http.Client
}
type Storage struct {
	mux                  sync.RWMutex
	tasks                map[uint64]*Task
	currentId            uint64
	requestProcessorChan chan *Task
}
type RequestStruct struct {
	Method  string
	Address string
}
type Task struct {
	mux           sync.RWMutex
	Checked       bool
	Id            uint64
	Url           string
	Method        string
	HttpStatus    int
	Headers       *http.Header
	LengthAnswers int64
	TimeStamp     time.Time
	Error         error
}

//Task methods
func (t *Task) setStatus(s int) {
	t.mux.Lock()
	defer t.mux.Unlock()

	t.HttpStatus = s
}
func (t *Task) setHeader(h *http.Header) {
	t.mux.Lock()
	defer t.mux.Unlock()

	t.Headers = h
}
func (t *Task) setLengthAnswer(l int64) {
	t.mux.Lock()
	defer t.mux.Unlock()

	t.LengthAnswers = l
}
func (t *Task) setTimeStamp() {
	t.mux.Lock()
	defer t.mux.Unlock()

	t.TimeStamp = time.Now()
}
func (t *Task) setError(err error) {
	t.mux.Lock()
	defer t.mux.Unlock()

	t.Error = err
}
func (t *Task) setChecked() {
	t.mux.Lock()
	defer t.mux.Unlock()

	t.Checked = true
}

//Storage methods
func (s *Storage) addItem(r *Task) uint64 {
	defer func() {
		s.mux.Unlock()
		s.nextId()
	}()

	r.Id = s.currentId

	s.mux.Lock()
	s.tasks[s.currentId] = r
	return s.currentId
}
func (s *Storage) nextId() {
	s.mux.Lock()
	defer s.mux.Unlock()

	atomic.AddUint64(&s.currentId, uint64(1))
}
func (s *Storage) getTasks(itemsOnPage int, page int) []*Task {
	s.mux.RLock()
	defer s.mux.RUnlock()

	posStart := page * itemsOnPage
	posFinish := posStart + itemsOnPage

	var outSlice []*Task
	for _, task := range s.tasks {
		outSlice = append(outSlice, task)
	}

	sort.Slice(outSlice, func(i, j int) bool {
		return outSlice[i].Id < outSlice[j].Id
	})

	sliceLen := len(outSlice)
	switch true {
	case posStart > sliceLen:
		return nil
	case posFinish > sliceLen:
		return outSlice[posStart:]
	}

	return outSlice[posStart:posFinish]
}
func (s *Storage) getUnCheckedTask() []*Task {
	s.mux.RLock()
	defer s.mux.RUnlock()

	var out []*Task
	for _, task := range s.tasks {
		if task.Checked == false {
			out = append(out, task)
		}
	}

	//if len(out) != 0 {
	fmt.Println("Get", len(out), "unchecked Task")
	//}
	return out
}
func (s *Storage) deleteTask(id uint64) error {
	s.mux.Lock()
	defer s.mux.Unlock()

	if _, ok := s.tasks[id]; ok == false {
		return errors.New("task not found")
	}

	delete(s.tasks, id)
	return nil
}
func (s *Storage) getTaskById(id uint64) (*Task, error) {
	s.mux.Lock()
	defer s.mux.Unlock()

	if _, ok := s.tasks[id]; ok == false {
		return nil, errors.New("task not found")
	}

	return s.tasks[id], nil
}

//AxonSoftTestApp methods
func (a *AxonSoftTestApp) init() {
	a.storage = Storage{
		tasks:                make(map[uint64]*Task),
		requestProcessorChan: make(chan *Task),
	}
	a.client = &http.Client{}
}

func (a *AxonSoftTestApp) postProcessor(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		return
	}

	var req RequestStruct

	if err := json.Unmarshal(body, &req); err != nil {
		log.Println(err)
		return
	}

	a.storage.addItem(&Task{
		Url:    req.Address,
		Method: req.Method,
	})

	if _, err := w.Write([]byte("task added to list \n")); err != nil {
		log.Println(err)
	}
}
func (a *AxonSoftTestApp) getProcessor(w http.ResponseWriter, r *http.Request) {
	p, keyErr := getIntUrlKey(r, "page")
	if keyErr != nil {
		if _, err := w.Write([]byte(keyErr.Error())); err != nil {
			log.Println(err)
		}
		return
	}

	t, keyErr := getIntUrlKey(r, "count")
	if keyErr != nil {
		if _, err := w.Write([]byte(keyErr.Error())); err != nil {
			log.Println(err)
		}
		return
	}

	taskList := a.storage.getTasks(t, p)
	if len(taskList) == 0 {
		if _, err := w.Write([]byte("No Tasks \n")); err != nil {
			log.Println(err)
		}
		return
	}

	for _, task := range taskList {
		if _, err := w.Write([]byte(fmt.Sprintf("id:%d; httpStatus:%d; url:%s; status:%t \n", task.Id, task.HttpStatus, task.Url, task.Checked))); err != nil {
			log.Println(err)
		}
	}
}
func (a *AxonSoftTestApp) deleteProcessor(w http.ResponseWriter, r *http.Request) {
	id, keyErr := getIntUrlKey(r, "id")
	if keyErr != nil {
		if _, err := w.Write([]byte(keyErr.Error())); err != nil {
			log.Println(err)
		}
		return
	}

	if err := a.storage.deleteTask(uint64(id)); err != nil {
		if _, err := w.Write([]byte(fmt.Sprintf("%s\n", err.Error()))); err != nil {
			log.Println(err)
		}
	}
}
func (a *AxonSoftTestApp) processor(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		a.postProcessor(w, r)
	case "GET":
		a.getProcessor(w, r)
	case "DELETE":
		a.deleteProcessor(w, r)
	}
}

func (a *AxonSoftTestApp) requestProcessor() {
	for {
		select {
		case task := <-a.storage.requestProcessorChan:
			resp, err := a.getResp(task)
			if err != nil {
				task.setError(err)
				return
			}
			task.setStatus(resp.StatusCode)
			task.setHeader(&resp.Header)
			task.setLengthAnswer(resp.ContentLength)
			task.setTimeStamp()
			task.setChecked()
		}
	}
}

func (a *AxonSoftTestApp) taskChecker() {
	for {
		for _, task := range a.storage.getUnCheckedTask() {
			a.storage.requestProcessorChan <- task
		}
	}
}

func (a *AxonSoftTestApp) getResp(task *Task) (*http.Response, error) {

	req, err := http.NewRequest(task.Method, task.Url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")

	resp, err := a.client.Do(req)
	defer func() {
		_ = resp.Body.Close()
	}()

	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return resp, nil
}

var App AxonSoftTestApp

func init() {
	App.init()
}
func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		App.processor(w, r)
	})

	go App.requestProcessor()
	go App.taskChecker()

	//TODO get port from os.ENV
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Println("fail start server: ", err.Error())
	}
}

//helpers functions
func getIntUrlKey(r *http.Request, key string) (int, error) {
	keys, ok := r.URL.Query()[key]

	if !ok || len(keys[0]) == 0 {
		return 0, errors.New(fmt.Sprintf("url param '%s' is missing", key))
	}

	n, err := strconv.Atoi(keys[0])
	if err != nil {
		return 0, errors.New("invalid format")
	}

	return n, nil
}
