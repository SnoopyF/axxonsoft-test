package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestProcessor(t *testing.T) {
	A := &AxonSoftTestApp{}
	A.init()

	postReq, err := http.NewRequest("POST", "", bytes.NewReader([]byte("{\"Method\":\"GET\", \"Address\":\"http://google.com\"}")))
	if err != nil {
		t.Fatal(err)
	}

	getReq, err := http.NewRequest("GET", "?page=1&count=1", nil)
	if err != nil {
		t.Fatal(err)
	}

	delReq, err := http.NewRequest("DELETE", "?id=1000", nil)
	if err != nil {
		t.Fatal(err)
	}

	testPostProcessor(t, A, postReq)
	testGetProcessor(t, A, getReq)
	testDeleteProcessor(t, A, delReq)

}

func testPostProcessor(t *testing.T, A *AxonSoftTestApp, req *http.Request) {
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(A.postProcessor)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := "task added to list \n"
	if body := rr.Body.String(); body != expected {
		t.Errorf("handler returned wrong body: got %v want %v",
			body, expected)
	}

}

func testGetProcessor(t *testing.T, A *AxonSoftTestApp, req *http.Request) {
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(A.getProcessor)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := "No Tasks \n"
	if body := rr.Body.String(); body != expected {
		t.Errorf("handler returned wrong body: got %v want %v",
			body, expected)
	}

}

func testDeleteProcessor(t *testing.T, A *AxonSoftTestApp, req *http.Request) () {
	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(A.deleteProcessor)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := "task not found\n"
	if body := rr.Body.String(); body != expected {
		t.Errorf("handler returned wrong body: got %v want %v",
			body, expected)
	}
}
